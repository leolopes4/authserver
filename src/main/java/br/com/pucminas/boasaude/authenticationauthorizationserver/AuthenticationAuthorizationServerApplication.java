package br.com.pucminas.boasaude.authenticationauthorizationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class AuthenticationAuthorizationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationAuthorizationServerApplication.class, args);
	}

}
