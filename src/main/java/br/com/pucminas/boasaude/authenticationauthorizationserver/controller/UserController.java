package br.com.pucminas.boasaude.authenticationauthorizationserver.controller;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.UserRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.service.AuthService;
import br.com.pucminas.boasaude.authenticationauthorizationserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")
public class UserController {

    private UserService userService;

    @Autowired
    UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping("/user")
    public void createUser(@RequestBody UserRequestDTO userRequestDTO){
        this.userService.createUser(userRequestDTO);

    }
}
