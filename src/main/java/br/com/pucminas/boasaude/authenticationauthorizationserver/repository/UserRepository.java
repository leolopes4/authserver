package br.com.pucminas.boasaude.authenticationauthorizationserver.repository;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.AuthEntity;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<List<UserEntity>> findByUsername(String username);

}
