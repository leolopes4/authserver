package br.com.pucminas.boasaude.authenticationauthorizationserver.repository;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.AuthEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<AuthEntity, Long> {

    Optional<AuthEntity> getAuthByToken(String Token);
}
