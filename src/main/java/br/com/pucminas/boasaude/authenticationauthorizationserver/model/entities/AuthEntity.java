package br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.enums.GrantTypeEnum;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "auth")
public class AuthEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private String grantType;

    @NonNull
    private String token;
}
