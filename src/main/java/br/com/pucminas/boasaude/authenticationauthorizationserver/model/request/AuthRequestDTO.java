package br.com.pucminas.boasaude.authenticationauthorizationserver.model.request;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.AuthEntity;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.enums.GrantTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class AuthRequestDTO {

    @NonNull
    private UserRequestDTO userRequestDTO;

    @NonNull
    private String grantTypeEnum;

    private String token;

    public AuthEntity toEntity(){
        return new AuthEntity(
                this.grantTypeEnum,
                this.token
        );
    }

}
