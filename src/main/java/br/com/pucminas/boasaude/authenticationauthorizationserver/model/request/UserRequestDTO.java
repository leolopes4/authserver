package br.com.pucminas.boasaude.authenticationauthorizationserver.model.request;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRequestDTO {
    private String username;
    private String password;

    public UserEntity toEntity(){
        return new UserEntity(
                this.username,
                this.password
        );
    }
}
