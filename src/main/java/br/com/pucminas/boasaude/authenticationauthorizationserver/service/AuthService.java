package br.com.pucminas.boasaude.authenticationauthorizationserver.service;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.AuthEntity;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.UserEntity;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.AuthRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.UserRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.response.AuthResponseDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.repository.AuthRepository;
import br.com.pucminas.boasaude.authenticationauthorizationserver.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

@Service
public class AuthService {

    private AuthRepository authRepo;
    private UserRepository userRepo;

    @Autowired
    AuthService(AuthRepository authRepo, UserRepository userRepo){
        this.authRepo = authRepo;
        this.userRepo = userRepo;
    }

    public Optional<AuthResponseDTO> generateToken(AuthRequestDTO authRequestDTO){
        boolean isValidUsed = this.hasValidUser(authRequestDTO.getUserRequestDTO());

        if(isValidUsed){
            String token = RandomStringUtils.randomAlphanumeric(64);
            authRequestDTO.setToken(token);

            AuthEntity authEntity = authRequestDTO.toEntity();

            try{
                this.authRepo.save(authEntity);

                return Optional.of(new AuthResponseDTO(token));
            }catch (DataAccessException exception){

            }
        };

        return Optional.empty();

    }

    public boolean isAuthorized(String token){
        Optional<AuthEntity> authEntity = this.authRepo.getAuthByToken(token);

        if (authEntity.isPresent()){
            return true;
        }else {
            return false;
        }

    }

    public boolean hasValidUser(UserRequestDTO userRequestDTO){
        Optional<List<UserEntity>> userEntity = this.userRepo.findByUsername(userRequestDTO.getUsername());

        if(userEntity.isPresent()){
            for (UserEntity user : userEntity.get()) {
                if(user.getPassword().equals(userRequestDTO.getPassword())){
                    return true;
                }else {
                    return false;
                }
            }
        }

        return false;
    }


}
