package br.com.pucminas.boasaude.authenticationauthorizationserver.controller;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.enums.GrantTypeEnum;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.AuthRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.UserRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.response.AuthResponseDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController implements APIAuthController {

    private AuthService authService;

    @Autowired
    AuthController(AuthService authService){
        this.authService = authService;
    }


    @GetMapping("/token")
    public ResponseEntity<?> generateToken(@RequestHeader("username") String username, @RequestHeader("password") String password, @RequestHeader("grant_type") String grants){
        AuthRequestDTO authRequestDTO = new AuthRequestDTO(new UserRequestDTO(username, password), grants);
        Optional<AuthResponseDTO> authResponseDTO = this.authService.generateToken(authRequestDTO);

        if (authResponseDTO.isPresent()){
            return ResponseEntity.ok().body(authResponseDTO.get());
        }{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @GetMapping("/validate")
    public ResponseEntity<?> authorizeToken(@RequestHeader("authorization") String bearertoken){
        String[] token = bearertoken.split(" ");
        boolean isAuthorized = this.authService.isAuthorized(token[1]);

        if(isAuthorized){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }


}
