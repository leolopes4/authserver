package br.com.pucminas.boasaude.authenticationauthorizationserver.model.enums;

public enum GrantTypeEnum {
    INFO_REGISTER("INFO-REGISTER");

    private String description;

    GrantTypeEnum(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
