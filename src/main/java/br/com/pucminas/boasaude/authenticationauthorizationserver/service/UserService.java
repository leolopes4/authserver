package br.com.pucminas.boasaude.authenticationauthorizationserver.service;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.entities.UserEntity;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.UserRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class UserService {

    private UserRepository userRepo;

    @Autowired
    UserService(UserRepository userRepo){
        this.userRepo = userRepo;
    }

    public void createUser(UserRequestDTO userRequestDTO){
        UserEntity userEntity = userRequestDTO.toEntity();

        this.userRepo.save(userEntity);

    }

}
