package br.com.pucminas.boasaude.authenticationauthorizationserver.controller;

import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.AuthRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.request.UserRequestDTO;
import br.com.pucminas.boasaude.authenticationauthorizationserver.model.response.AuthResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Optional;

@Api(tags = "AuthDocumentation")
public interface APIAuthController {

    @ApiOperation(value = "Generate Token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", required = true, allowEmptyValue = false, paramType = "header", example = "lpess"),
            @ApiImplicitParam(name = "password", required = true, allowEmptyValue = false, paramType = "header", example = "teste@asd"),
            @ApiImplicitParam(name = "grants", required = true, allowEmptyValue = false, paramType = "header", example = "info-register")
    })
    public ResponseEntity<?> generateToken(String username, String password, String grants);

    @ApiOperation(value = "Validate Token")
    @ApiImplicitParam(name = "Authorization", value = "Bearer Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer emB9fS8AV4b8xf6jIZzZQD3e1NOyLMWpd9wuMyj4BnpXEDkd8rEgB6ONtIeUH7MB")
    public ResponseEntity<?> authorizeToken(String bearertoken) ;

}
